class puppet_base {
  file { '/etc/puppet/hiera.yaml':
    ensure => 'present',
    source => 'puppet:///files/hiera.yaml';
  }
}

class install_rvm {
  include rvm
  rvm::system_user {vagrant: ;}

  $rubies = ['ruby-1.9.3-p448', 'ruby-1.8.7-p357']
  rvm_system_ruby { $rubies:
    ensure => 'present'
  }

  $gemsets = $rubies.collect |$ruby| { "${ruby}@default"}
  rvm_gemset { $gemsets:
    ensure => 'present',
    require => Rvm_system_ruby[$rubies];
  }

  $bundlers = $gemsets.collect |$gemset| { "${gemset}/bundler" }
  rvm_gem { $bundlers:
    ensure => '1.0.21',
    require => Rvm_gemset[$gemsets];
  }
}

class mysql {
  $password = "root123"

  $mysql = ['mysql-server', 'libmysqlclient-dev']

  package { $mysql:
    ensure => 'present'
  }

  exec { "Set MySQL server root password":
    subscribe => Package[$mysql],
    refreshonly => true,
    unless => "mysqladmin -uroot -p$password status",
    path => "/bin:/usr/bin",
    command => "mysqladmin -uroot password $password",
  }
}

class postgresql {
  $postgresql = ['postgresql', 'libpq-dev']

  package { $postgresql:
    ensure => 'present'
  }
}

class vcs {
  $vcs = ['git', 'git-svn']

  package {$vcs:
    ensure => 'installed'
  }
  # @note this code in in not properly installing
  exec { 'Install SCM breez for better workflow':
    creates => '/home/vagrant/.scm_breeze',
    path => '/bin:/usr/bin',
    command => 'git clone git://github.com/ndbroadbent/scm_breeze.git /home/vagrant/.scm_breeze && /home/vagrant/.scm_breeze/install.sh',
    #owner => 'vagrant', group => 'vagrant',
    require => Package[$vcs];
  }
}

class review {
  File { owner => 'vagrant', group => 'vagrant', mode => 0755 }
  file { '/home/vagrant/review.py':
    ensure => 'present',
    source => 'puppet:///files/review.py';
  }
}

class spade {
  group { "puppet":
    ensure => "present",
  }

  File { owner => 0, group => 0, mode => 0644 }

  file { '/etc/motd':
    content => "Welcome to your Vagrant-built virtual machine!
    Managed by Puppet.\n"
  }

  file { '/etc/hosts':
    ensure => 'present',
    source => 'puppet:///files/hosts';
  }

  $build_utils = [
                  # 'libmagick++-dev',
                  'rpcbind',
                  'nfs-common'
                  ]
  # $dbs = ['mongodb', 'mongodb-dev']
  # $amqp = ['rabbitmq-server', 'librabbitmq-dev']
  $mux = ['tmux']

  package {[
            $build_utils,
            #$dbs,
            #$amqp,
            $mux
            ]:
    ensure => 'installed'
  }

  include puppet_base
  include postgresql
  include install_rvm
  # include vcs
}

include spade
