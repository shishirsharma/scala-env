distro=`lsb_release -i | cut -d":" -f2`

if [ $distro = "Ubuntu" ]; then
    dpkg -s puppet >/dev/null  2>&1
    if [ $? -eq 0 ]; then
        current_puppet_version=`dpkg -s puppet| grep Version | cut -d":" -f2`
    else
        current_puppet_version=`puppet -V`
    fi
    dpkg --compare-versions $current_puppet_version lt "3.2"
    if [ $? -eq 0 ]; then
        echo "Updating Puppet to 3.2"
        [ $? -eq 0 ] && echo "Downloading..." && rm -rf puppetlabs-release-precise.*
        [ $? -eq 0 ] && wget http://apt.puppetlabs.com/puppetlabs-release-precise.deb > /dev/null 2>&1
        [ $? -eq 0 ] && echo "Adding repository sources..."
        [ $? -eq 0 ] && dpkg -i puppetlabs-release-precise.deb
        [ $? -eq 0 ] && echo "Updting sources..."
        [ $? -eq 0 ] && apt-get update > /dev/null  2>&1
        [ $? -eq 0 ] && echo "Installing puppet..."
        [ $? -eq 0 ] && apt-get -y install puppet > /dev/null  2>&1
        [ $? -eq 0 ] && echo "Cleanup..."
        [ $? -eq 0 ] && rm -rf puppetlabs-release-precise.deb
    else
        echo "Puppet already on 3.2"
    fi
else
    # Untested code
    [ `rpm -qa puppetlabs-release` = 'puppetlabs-release-6-7.noarch' ] || rpm -ivh http://yum.puppetlabs.com/el/6/products/i386/puppetlabs-release-6-7.noarch.rpm
    [ `rpm -qa puppet` = 'puppet-3.2.2-1.el6.noarch' ] || yum -y update-to puppet-3.2.2
fi
